#include "librttex.h"
#include <zlib.h>
#include <png.h>
#include <stdlib.h>
#include <string.h>

#define RT_IS_VALID(fdata)	(fdata != NULL && fdata->data != NULL)


// RTFONT STRUCTS //

struct rtfont_header
{
    char file_type[6];
    char version;
    char reserved_1;
    
    short char_spacing;
    short line_height;
    short line_spacing;
    short shadow_offset_x;
    
    short shadow_offset_y;
    short first_char;
    short last_char;
    short blank_char_width;
    
    short color_trigger_count;
    short kerning_pair_count;
    char reserved_2[124];
};

struct rtfont_char_data
{
    short bmp_pos_x;
    short bmp_pos_y;
    short char_size_x;
    short char_size_y;
    
    short char_bmp_offset_x;
    short char_bmp_offset_y;
    float char_bmp_pos_u;
    
    float char_bmp_pos_v;
    float char_bmp_pos_u2;
    
    float char_bmp_pos_v2;
    short xadvance;
    // memory will probably add 2 more bytes HERE for alignment
    // wont be a problem unless we deliberately compile it without this optimization
};

struct rtfont_kerning_pair
{
    // A kerning pair is a pair of two letters (like r & n) to which we add an additional space
    // Pairs like rn would otherwise look like an m, for example
    short left_char;
    short right_char;
    signed char space;
    // probably 3 more bytes for alignment
};

struct rtfont_color_trigger
{
    // Specifies the color to apply when writing " `n " , where n == trigger_char
    unsigned int color;
    char trigger_char;
    // probably 3 more bytes for alignment
};



// VARIABLES //

static const char* RTPACK = "RTPACK";
static const char* RTTXTR = "RTTXTR";
static const char* RTFONT = "RTFONT";



// PROTOTYPES //

static int
rt_read_int(char* start);

static int
rt_get_compressed_size(RT_FILE_DATA* fdata);

static int
rt_get_decompressed_size(RT_FILE_DATA* fdata);



// PUBLIC FUNCTIONS //

RT_FILE_DATA*
rt_read_file(char* file_path)
{
    FILE* file = fopen(file_path, "rb");
    if (file == NULL)
        return NULL;


    // Get file size
    fseek(file, 0, SEEK_END);
    int file_size = ftell(file);
    fseek(file, 0, SEEK_SET);


    // Create fdata
    RT_FILE_DATA* fdata = malloc(sizeof(RT_FILE_DATA));
    if (fdata == NULL)
        return NULL;

    fdata->data = malloc(file_size);
    if (fdata->data == NULL)
    {
        free(fdata);
        return NULL;
    }

    fdata->size = file_size;


    // Read file
    for (int i = 0; i < file_size; i++)
    {
        int c = fgetc(file);
        if (c == EOF)
        {
            free(fdata->data);
            free(fdata);
            return NULL;
        }
        
        fdata->data[i] = (char)c;
    }


    fclose(file);

    // Done
    return fdata;
}


void
rt_free(RT_FILE_DATA* fdata)
{
    if (fdata != NULL)
    {
        if (fdata->data != NULL)
            free(fdata->data);
        
        free(fdata);
    }
}


char
rt_is_rtpack(RT_FILE_DATA* fdata)
{
    if (!RT_IS_VALID(fdata))
        return FALSE;

    if (fdata->size < 6)
        return FALSE;

    for (int i = 0; i < 6; i++)
    {
        if (RTPACK[i] != fdata->data[i])
            return FALSE;
    }

    return TRUE;
}


char
rt_is_rttex(RT_FILE_DATA* fdata)
{
    if (!RT_IS_VALID(fdata))
        return FALSE;

    if (fdata->size < 6)
        return FALSE;

    for (int i = 0; i < 6; i++)
    {
        if (RTTXTR[i] != fdata->data[i])
            return FALSE;
    }

    return TRUE;
}


char
rt_is_rtfont(RT_FILE_DATA* fdata)
{
    if (!RT_IS_VALID(fdata) || fdata->size < 6)
        return FALSE;
    
    for (int i = 0; i < 6; i++)
    {
        if (RTFONT[i] != fdata->data[i])
            return FALSE;
    }
    
    return TRUE;
}


char
rt_rtpack_unpack(RT_FILE_DATA* fdata)
{
    if (!RT_IS_VALID(fdata) || fdata->size <= 32) // Header is 32 bytes
        return FALSE;

    // Prepare
    int output_size = rt_get_decompressed_size(fdata);
    char* output = malloc(output_size);
    if (output == NULL)
        return FALSE;

    z_stream stream = {
        .next_in = (char*)(fdata->data + 32), // start outside of header
        .avail_in = (fdata->size - 32),
        .next_out = output,
        .avail_out = output_size,
        
        .zalloc = NULL,
        .zfree = NULL,
        .opaque = NULL
    };

    // Init inflate
    int status = inflateInit(&stream);
    if (status != Z_OK)
    {
        free(output);
        return FALSE;
    }

    // Inflate (decompress)
    status = inflate(&stream, Z_NO_FLUSH);

    // Error?
    if (status != Z_STREAM_END)
    {
        inflateEnd(&stream);
        free(output);
        return FALSE;
    }

    // Done
    inflateEnd(&stream);

    // Apply
    free(fdata->data);
    fdata->data = output; // output contains the new rttex header
    fdata->size = output_size;

    return TRUE;
}


int
rt_get_width(RT_FILE_DATA* fdata)
{
    if (!RT_IS_VALID(fdata) || !rt_is_rttex(fdata))
        return 0;

    return rt_read_int(&fdata->data[24]);
}


int
rt_get_occupied_width(RT_FILE_DATA* fdata)
{
    if (!RT_IS_VALID(fdata) || !rt_is_rttex(fdata))
        return 0;

    return rt_read_int(&fdata->data[12]);
}


int
rt_get_height(RT_FILE_DATA* fdata)
{
    if (!RT_IS_VALID(fdata) || !rt_is_rttex(fdata))
        return 0;

    return rt_read_int(&fdata->data[20]);
}


int
rt_get_occupied_height(RT_FILE_DATA* fdata)
{
    if (!RT_IS_VALID(fdata) || !rt_is_rttex(fdata))
        return 0;

    return rt_read_int(&fdata->data[8]);
}


void
rt_get_pixel(   RT_FILE_DATA*   fdata,
                int             x,
                int             y,
                float*          red,
                float*          green,
                float*          blue,
                float*          alpha)
{
    int width = rt_get_occupied_width(fdata);
    int height = rt_get_occupied_height(fdata);

    char has_alpha = fdata->data[28];
    int channels = (has_alpha ? 4 : 3);

    char* ptr = fdata->data;
    ptr += fdata->size - width * height * channels; // TODO: alpha check
    ptr += (width * y + x) * channels;

    *red = (float)ptr[0] / 255.0f;
    *green = (float)ptr[1] / 255.0f;
    *blue = (float)ptr[2] / 255.0f;
    *alpha = (float)ptr[3] / 255.0f;
}


unsigned char*
rt_create_copy( RT_FILE_DATA*   fdata,
                int*            outWidth,
                int*            outHeight,
                int*            outChannels)
{
    int occWidth = rt_get_occupied_width(fdata);
    int occHeight = rt_get_occupied_height(fdata);
    int width = rt_get_width(fdata);
    int height = rt_get_height(fdata);
    
    char has_alpha = fdata->data[28];
    int channels = (has_alpha ? 4 : 3);
    
    char* ptr = fdata->data;
    ptr += fdata->size - occWidth * occHeight * channels;
    
    unsigned char* buffer = (unsigned char*)malloc(width * height * channels);
    if (!buffer)
        return NULL;
    
    for (int i = 0; i < height; i++)
    {
        memcpy(buffer, ptr, width * channels);
        ptr += occWidth * channels;
    }
    
    *outWidth = width;
    *outHeight = height;
    *outChannels = channels;
    
    return buffer;
}


char
rt_make_png_cropped(    RT_FILE_DATA*   fdata,
                        char*           out_path,
                        int             out_width,
                        int             out_height)
{
    if (out_width < 1 || out_height < 1)
        return FALSE;

    // w == writing; b avoids translations such as "\n" to "\r\n"
    FILE* png = fopen(out_path, "wb");
    if (png == NULL)
        return FALSE;

    // Initialize PNG writer & info
    png_structp write_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (write_ptr == NULL)
    {
        fclose(png);
        return FALSE;
    }

    png_infop info_ptr = png_create_info_struct(write_ptr);
    if (info_ptr == NULL)
    {
        fclose(png);
        png_destroy_write_struct(   &write_ptr,
                                    (png_infopp)NULL);
        return FALSE;
    }

    // Errors?
    // (libpng will jump in here if there were any. status will return != 0 in this case)
    // (setjmp && longjmp are like goto, but long)
    int status = setjmp(png_jmpbuf(write_ptr));
    if (status != 0)
    {
        fclose(png);
        png_destroy_write_struct(&write_ptr, &info_ptr);
        return FALSE;
    }

    // Prepare I/O
    png_init_io(write_ptr, png);

    png_set_IHDR(	write_ptr,
                    info_ptr,
                    out_width,
                    out_height,
                    8, // 8 bits per channel
                    PNG_COLOR_TYPE_RGB_ALPHA,
                    PNG_INTERLACE_NONE,
                    PNG_COMPRESSION_TYPE_DEFAULT,
                    PNG_FILTER_TYPE_DEFAULT);

    png_write_info(write_ptr, info_ptr);

    // Get delta height for cutting
    int dt_height = rt_get_occupied_height(fdata) - out_height;

    // Get image as png_byte-array
    png_byte* row_pointers[out_height];
    for (int y = 0; y < out_height; y++)
    {
        png_byte* row = malloc(out_width * 4);
        
        for (int x = 0; x < out_width; x++)
        {
            float r, g, b, a;
            rt_get_pixel(fdata, x, y + dt_height, &r, &g, &b, &a);
            
            row[x * 4] = (char)(r * 255.0f);
            row[x * 4 + 1] = (char)(g * 255.0f);
            row[x * 4 + 2] = (char)(b * 255.0f);
            row[x * 4 + 3] = (char)(a * 255.0f);
        }
        
        row_pointers[out_height - y - 1] = row;
    }

    png_write_image(write_ptr, row_pointers);
    png_write_end(write_ptr, info_ptr);


    png_destroy_write_struct(&write_ptr, &info_ptr);

    for (int y = 0; y < out_height; y++)
        free(row_pointers[y]);

    fflush(png);
    fclose(png);
    return TRUE;
}


char
rt_make_png(    RT_FILE_DATA*   fdata,
                char*           out_path)
{
    return rt_make_png_cropped( fdata,
                                out_path,
                                rt_get_width(fdata),
                                rt_get_height(fdata));
}



// RTFONT FUNCTIONS //

int
rt_font_get_character_count(RT_FILE_DATA* fdata)
{
    if (!rt_is_rtfont(fdata))
        return 0;
    
    struct rtfont_header* header_ptr = (struct rtfont_header*)fdata->data;
    return header_ptr->last_char - header_ptr->first_char;
}

char
rt_font_get_character(  RT_FILE_DATA*   fdata,
                        int             idx,
                        short*          char_id,
                        int*            map_x,
                        int*            map_y,
                        int*            map_w,
                        int*            map_h)
{
    if (!rt_is_rtfont(fdata) || idx < 0 || idx >= rt_font_get_character_count(fdata))
        return FALSE;
    
    struct rtfont_header* header_ptr = (struct rtfont_header*)fdata->data;
    struct rtfont_char_data* char_ptr = (struct rtfont_char_data*)(header_ptr + 1);
    
    for (int i = 0; i < idx; i++)
        char_ptr++;
    
    
    if (char_id)
        *char_id = header_ptr->first_char + idx;
    
    if (map_x)
        *map_x = char_ptr->bmp_pos_x;
    if (map_y)
        *map_y = char_ptr->bmp_pos_y;
    
    if (map_w)
        *map_w = char_ptr->char_size_x;
    if (map_h)
        *map_h = char_ptr->char_size_y;
    
    return TRUE;
}

char
rt_font_make_png(   RT_FILE_DATA*   fdata,
                    char*           output_path)
{
    if (!rt_is_rtfont(fdata))
        return FALSE;
    
    // Check if file exists already
    FILE* file = fopen(output_path, "rb");
    if (file)
    {
        fclose(file);
        return FALSE;
    }
    
    
    // Get pointers
    struct rtfont_header* header_ptr = (struct rtfont_header*)fdata->data;
    struct rtfont_char_data* char_ptr = (struct rtfont_char_data*)(header_ptr + 1);
    
    
    // Get maximum positions in image
    short max_x = 0, max_y = 0;
    struct rtfont_char_data* ph_char_ptr = char_ptr;
    for (int i = 0; i < rt_font_get_character_count(fdata); i++)
    {
        short pos_x = ph_char_ptr->bmp_pos_x + ph_char_ptr->char_size_x;
        short pos_y = ph_char_ptr->bmp_pos_y + ph_char_ptr->char_size_y;
        
        max_x = (max_x < pos_x ? pos_x : max_x);
        max_y = (max_y < pos_y ? pos_y : max_y);
        
        ph_char_ptr++;
    }
    
    
    // Get width/height rounded up in 128's steps
    int width = max_x + 128 - (max_x % 128);
    int height = max_y + 128 - (max_y % 128);
    
    // ..check if its all good
    if (max_x < 1 || max_y < 1)
        return FALSE;
    
    
    // Get pixel data pointer
    unsigned char* bmp = (unsigned char*)header_ptr + fdata->size - width * height * 4; // probably always 4 channels
    
    
    // Create PNG
    {
        FILE* png = fopen(output_path, "wb");
        if (!png)
            return FALSE;

        // Initialize PNG writer & info
        png_structp write_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
        if (!write_ptr)
        {
            fclose(png);
            return FALSE;
        }

        png_infop info_ptr = png_create_info_struct(write_ptr);
        if (!info_ptr)
        {
            png_destroy_write_struct(&write_ptr, (png_infopp)NULL);
            fclose(png);
            return FALSE;
        }

        // Errors?
        int status = setjmp(png_jmpbuf(write_ptr));
        if (status != 0)
        {
            png_destroy_write_struct(&write_ptr, &info_ptr);
            fclose(png);
            return FALSE;
        }

        // Prepare I/O
        png_init_io(write_ptr, png);

        png_set_IHDR(	write_ptr,
                        info_ptr,
                        width,
                        height,
                        8, // 8 bits per channel
                        PNG_COLOR_TYPE_RGB_ALPHA,
                        PNG_INTERLACE_NONE,
                        PNG_COMPRESSION_TYPE_DEFAULT,
                        PNG_FILTER_TYPE_DEFAULT);

        png_write_info(write_ptr, info_ptr);

        // Get image as png_byte-array
        png_byte* row_pointers[height];
        for (int y = 0; y < height; y++)
        {
            png_byte* row = malloc(width * 4);
            
            for (int x = 0; x < width; x++)
            {
                int idx = (x + y * width) * 4;
                
                row[x * 4] = bmp[idx];
                row[x * 4 + 1] = bmp[idx + 1];
                row[x * 4 + 2] = bmp[idx + 2];
                row[x * 4 + 3] = bmp[idx + 3];
            }
            
            row_pointers[height - y - 1] = row;
        }

        png_write_image(write_ptr, row_pointers);
        png_write_end(write_ptr, info_ptr);


        png_destroy_write_struct(&write_ptr, &info_ptr);

        for (int y = 0; y < height; y++)
            free(row_pointers[y]);

        fflush(png);
        fclose(png);
    }
    
    
    // Done
    return TRUE;
}



// PRIVATE FUNCTIONS //
static int
rt_read_int(char* start)
{
    // (Little Endian: 3F|17|00|00 -> 0000173F hex)

    int num = 0;
    for (int i = 0; i < 4; i++)
    {
        num = (num << 8) | (unsigned char)start[3 - i];
    }

    return num;
}


static int
rt_get_compressed_size(RT_FILE_DATA* fdata)
{
    return rt_read_int(&fdata->data[8]);
}


static int
rt_get_decompressed_size(RT_FILE_DATA* fdata)
{
    return rt_read_int(&fdata->data[12]);
}
