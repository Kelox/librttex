# librttex

Read Header for API

## Compile

Compile with `gcc -c librttex.c`

Create static lib with `ar crs librttex.a librttex.o`

To compile a program that's using this lib: `gcc OBJ_FILES -lz -lpng -lrttex -o out`

## Install

Put `librttex.a` into `/usr/local/lib/`
and `librttex.h` into `/usr/local/include/`

## Else

Make sure to call `free(rt_file_data.data);` after you're done
