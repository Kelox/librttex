#ifndef LIBRTTEX_H
#define LIBRTTEX_H


#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif


typedef struct _rt_file_data
{
    unsigned char*  data;
    int             size;
} RT_FILE_DATA;



// Returns RT_FILE_DATA or NULL
RT_FILE_DATA*
rt_read_file(char* file_path);

// Frees all data occupied by RT_FILE_DATA
void
rt_free(RT_FILE_DATA* fdata);

// Returns TRUE or FALSE
char
rt_is_rtpack(RT_FILE_DATA* fdata);

// Returns TRUE or FALSE
char
rt_is_rttex(RT_FILE_DATA* fdata);

// Returns TRUE or FALSE
char
rt_is_rtfont(RT_FILE_DATA* fdata);

// Turns rtpack into rttex
// Returns TRUE if success
char
rt_rtpack_unpack(RT_FILE_DATA* fdata);

// Returns texture width or 0
int
rt_get_width(RT_FILE_DATA* fdata);

// Returns reserved texture width or 0 (always bigger than actual width & multiple of 2)
int
rt_get_occupied_width(RT_FILE_DATA* fdata);

// Returns texture height or 0
int
rt_get_height(RT_FILE_DATA* fdata);

// Returns reserved texture height or 0 (always bigger than actual height & multiple of 2)
int
rt_get_occupied_height(RT_FILE_DATA* fdata);

// Gets color. Range: [0; 1]
// Pass NULL if you want to skip a channel
void
rt_get_pixel(	RT_FILE_DATA*   fdata,
                int             x,
                int             y,
                float*          red,
                float*          green,
                float*          blue,
                float*          alpha);

// Creates a new buffer with only the raw image data
// Width & height will be written into outWidth and outHeight
// The amount of color channels will be written into outChannels (rgb or rgba)
unsigned char*
rt_create_copy( RT_FILE_DATA*   fdata,
                int*            outWidth,
                int*            outHeight,
                int*            outChannels);

// Turns rttex into png
// Must be at least 1x1
// Returns TRUE if success
char
rt_make_png_cropped(    RT_FILE_DATA*   fdata,
                        char*           out_path,
                        int             out_width,
                        int             out_height);

// Turns rttex into png
// Takes rt_get_width() & rt_get_height() as size
// Returns TRUE if success
char
rt_make_png(    RT_FILE_DATA*   fdata,
                char*           out_path);



// EXPERIMENTAL RTFONT FUNCTIONS //
// (Lacks a lot of functionality & barely works)

// Returns the amount of characters available
int
rt_font_get_character_count(RT_FILE_DATA* fdata);

// Returns the type & location of a character at index idx
// Coordinates are relative to the top-left corner
// Returns TRUE on success
char
rt_font_get_character(  RT_FILE_DATA*   fdata,
                        int             idx,
                        short*          char_id,
                        int*            map_x,
                        int*            map_y,
                        int*            map_w,
                        int*            map_h);

// Creates a PNG texture of all characters
// Returns TRUE on success
char
rt_font_make_png(   RT_FILE_DATA*   fdata,
                    char*           output_path);



#endif
